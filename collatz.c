#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<unistd.h>

int main(int argc, char**argv)
{

	
	int number;
	printf("START\n");

	number=atoi(argv[1]);

if(fork()==0)
{
	while(number>1)
	{
	printf("%d\n",number);
	if(number%2==0)
	number=number/2;
	else 
	number=3*number+1;
}

	printf("1\n");
}
else
{
	wait(NULL);
	printf("END\n");
}

return 0;
}
