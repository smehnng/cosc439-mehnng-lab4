#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include<stdlib.h>

int main()
{

//Declear variables
int number;
char str[5];

//Enter a number
	printf("Enter an integer between 1-100\n");
	scanf("%d",&number);
	printf("You entered %d\n",number);

//Change number into string to pass the arugment from collatz file 
	sprintf(str,"%d",number);
	int child=fork();
	
//child process
if(child==0)
{
	char*argc[]={"./num",str,NULL};//"./num" came from collatz.c
	execvp(argc[0],argc);
}
else
{
	wait(NULL);
}
return 0;

}
